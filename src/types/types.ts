import {IBrand, ICategories, IEdit, IProduct} from "../app/feaures/types/types";
export interface IState{
    click: {
        click: boolean
    }
    category: {
        product: ICategories[]
        total:number
    }
    product: {
        product: IProduct[]
        total:number
    }
    brand: {
        product: IBrand[]
        total: number
    }
    edit: IEdit
}