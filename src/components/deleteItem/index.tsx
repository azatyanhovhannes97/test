import React from "react";

export const deleteItem = (id: number | undefined,url: string) => {
   return  fetch(`${process.env.REACT_APP_BASE_URL}/${url}/${id}`,{
        method: 'DELETE'
    })
        .then(res => res.json())
}

