import React from "react";
import {IProps} from "./types";
import {IBrand} from "../../../app/feaures/types/types";
import {FaPen} from "@react-icons/all-files/fa/FaPen";
import {FaTrash} from "@react-icons/all-files/fa/FaTrash";
import {deleteItem} from "../../deleteItem";
import {editItem} from "../../editItem";
import {useNavigate} from "react-router-dom";
import GetBrand from "../../../hooks/GetBrand";


const getItem = (item: IBrand, key: keyof IBrand) => {
    return String(item[key])
}

const BrandTable: React.FC<IProps> = (props: IProps) => {
    const {sendRequest} = GetBrand()

    const nav = useNavigate()

    return<>
        <table className="table">
            <thead>
            <tr>
                {
                    props.thead.map((item, index) => {
                        return <React.Fragment key={index}><th scope="col">{item.split('.')[0]}</th></React.Fragment>
                    })
                }

            </tr>
            </thead>
            <tbody>
            {
                props.product.map((item, index) => {
                    return<React.Fragment key={index}><tr>
                        {
                            props.thead.map((key1, index1) => {
                                if (key1 === 'image'){
                                    return <React.Fragment key={index1}><td><img src={getItem(item, key1 as keyof IBrand)} height={'40px'} width={'40px'}/></td></React.Fragment>
                                } else if (key1 === 'action'){
                                    return <React.Fragment key={index1}><td><FaPen className={'icon'} onClick={() => {
                                        nav(`/editBrand/${item.id}`)
                                    }}/> <FaTrash className={'icon'} onClick={() => {
                                        deleteItem(item.id,'brands').then(() => {
                                            sendRequest()
                                        })
                                    }}/></td></React.Fragment>
                                }
                                return <React.Fragment key={index1}><td>{getItem(item,key1 as keyof IBrand)}</td></React.Fragment>
                            })
                        }
                    </tr></React.Fragment>
                })
            }
            </tbody>
        </table>
    </>
}

export default BrandTable