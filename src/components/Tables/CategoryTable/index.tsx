import React, {useEffect, useState} from "react";
import {IProps} from "./types";
import {ICategories} from "../../../app/feaures/types/types";
import {FaPen} from "@react-icons/all-files/fa/FaPen";
import {FaTrash} from "@react-icons/all-files/fa/FaTrash";
import {deleteItem} from "../../deleteItem";
import {editItem} from "../../editItem";
import {useNavigate} from "react-router-dom";
import GetCategory from "../../../hooks/GetCategory";


const getItem = (item: ICategories, key: keyof ICategories) => {
    return String(item[key])
}

const CategoryTable: React.FC<IProps> = (props:IProps) => {

    const {sendRequest} = GetCategory()

    const nav = useNavigate()

    return<>
        <table className="table">
            <thead>
            <tr>
                {
                    props.thead.map((item, index) => {
                        return <React.Fragment key={index}><th scope="col">{item.split('.')[0]}</th></React.Fragment>
                    })
                }

            </tr>
            </thead>
            <tbody>
            {
                props.product.map((item, index) => {
                    return <React.Fragment key={index}>
                        <tr>
                            {
                                props.thead.map((key1,index1) => {
                                 if (key1 === 'action'){
                                        return <React.Fragment key={index1}><td><FaPen className={'icon'} onClick={() => {
                                            nav(`/editCategory/${item.id}`)
                                        }}/> <FaTrash className={'icon'} onClick={() => {
                                            deleteItem(item.id,"categories").then(() => {
                                                sendRequest()
                                            })
                                        }}/></td></React.Fragment>
                                    }
                                    return<React.Fragment key={index1}>
                                        <td>
                                            {getItem(item,key1 as keyof ICategories)}
                                        </td>
                                    </React.Fragment>
                                })
                            }
                        </tr>
                    </React.Fragment>
                })
            }
            </tbody>
        </table>
    </>
}

export default CategoryTable