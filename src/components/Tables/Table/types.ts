import {IBrand, ICategories, IProduct} from "../../../app/feaures/types/types";

export interface IProps {
    thead: string[]
    product: IProduct[]
    symbols: string[]
}