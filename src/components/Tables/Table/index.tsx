import "./style.css"
import React, {useEffect, useState} from "react";
import {IProps} from "./types";
import {ICategories, IProduct} from "../../../app/feaures/types/types";
import {FaPen} from "@react-icons/all-files/fa/FaPen";
import {FaTrash} from "@react-icons/all-files/fa/FaTrash";
import {deleteItem} from "../../deleteItem";
import {editItem} from "../../editItem";
import {useNavigate} from "react-router-dom";
import GetData from "../../../hooks/getData";

const a = 0
type Dynamic = {[key: string]: string | number}

const getItem = (item:IProduct , key: keyof IProduct ) =>{
    const slpiting = key.split('.');
   if (slpiting.length > 1){
       const [key1,key2] = slpiting;
       const el:Dynamic  = item[key1 as keyof IProduct] as Dynamic;
       return String(el[key2])
   }
    return String(item[key])
}

const Table: React.FC<IProps> = (props:IProps) => {
    const nav = useNavigate()
    const {sendRequest} = GetData()
    return<>
        <table className="table">
            <thead>
            <tr>
                {
                    props.thead.map((item, index) => {
                       return <React.Fragment key={index}><th scope="col">{item.split('.')[0]}</th></React.Fragment>
                    })
                }

            </tr>
            </thead>
            <tbody>

            {
                props.product.map((item ,index ) => {
                    return <React.Fragment key={index}><tr>
                        {
                            props.thead.map((key1,index1: number) => {
                                if (key1 === 'Brand.Image'){
                                    return <React.Fragment key={index1}>
                                        <td>{props.symbols[index1]}<img src={getItem(item, key1.toLowerCase() as keyof IProduct)} height={'40px'} width={'40px'}/></td>
                                    </React.Fragment>
                                }else if (key1 === 'Image'){
                                    return<React.Fragment key={index1}>
                                        <td>{props.symbols[index1]}<img src={item.image} height={'40px'} width={'40px'}/></td>
                                    </React.Fragment>
                                }else if(key1 === 'Category.Name'){
                                    let a: ICategories[] | undefined = item.categories;
                                            return <React.Fragment key={index1}><td>{(a || [{name: undefined}])[0]?.name}</td></React.Fragment>
                                }else if (key1 === 'Action'){
                                    return <React.Fragment key={index1}><td><FaPen className={'icon'} onClick={() => {

                                        nav(`/editProduct/${item.id}`)
                                    }}/> <FaTrash className={'icon'} onClick={() => {
                                        deleteItem(item.id,'products').then(() => {
                                            sendRequest()
                                        })
                                    }}/></td></React.Fragment>
                                }
                                return<React.Fragment key={index1}>
                                    <td>{props.symbols[index1]}{getItem(item, key1.toLowerCase() as keyof IProduct)}</td>
                                </React.Fragment>
                            })
                        }
                    </tr></React.Fragment>
                })
            }



            </tbody>
        </table>
    </>
}

export default Table