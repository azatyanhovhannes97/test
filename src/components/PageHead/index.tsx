import "./style.css"
import React from "react";
import {FaSearch} from "@react-icons/all-files/fa/FaSearch";
import {IProps} from "./types";
import {useNavigate} from "react-router-dom";

const PageHead: React.FC<IProps> = (props:IProps) => {
    const nav = useNavigate()


    return<>
        {
            !props.input && !props.button ? <>
        <div className={'col-12 col-sm-12 col-md12'}>
            <h3>{props.head}</h3>
        </div>
            </> : <>
                <div className={'col-12 col-sm-12 col-md12'}>
                    <h3>{props.head}</h3>
                </div>
                <div className={'col-12 col-sm-12 col-md12'}>
                    <div className={'row'}>
                        <div className={'col-12 col-sm-12 col-md-8 searchDiv1'}>
                            <FaSearch className={'search '}/>
                            <input type="text" placeholder={`Search ${props.input}...`} className={'pt-3'}/>
                        </div>
                        <div className={'col-12 col-sm-12 col-md-4 flex end'}>
                            <button type="button" className="resButton btn btn-primary btn-lg" onClick={() => { nav(props.to || '/') } }>+ {props.button} {props.input}</button></div>
                    </div>
                </div>
            </>
    }
    </>
}

export default PageHead