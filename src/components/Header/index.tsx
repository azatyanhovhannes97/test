import "./style.css"
import React, {useEffect, useMemo, useState} from "react";
import Browser from "../svg/browser";
import { FaSearch } from "@react-icons/all-files/fa/FaSearch";
import {FaBell} from "@react-icons/all-files/fa/FaBell";
import Mobile from "../svg/Mobile";
import { useSelector,useDispatch } from "react-redux";
import {changePar} from "../../app/feaures/clickState/clickState";
import {IState} from "../../types/types";



const Header: React.FC = () => {
    const click = useSelector((state: IState) => state.click.click)
    const dispatch = useDispatch()



    return<>
         <div className={'header'}>
            <div className={'container'}>
                <div className={'row'}>
                    <div className={'col-8 col-sm-6 col-md-6   p-3'}>
                        <div className={'pt-2 flex'}>
                            <Mobile className={'mobile'} onClick={() => {
                                dispatch(changePar(!click))
                            }
                            }/>
                            <Browser className={'Br_icon'}/>
                            <p>Browse Website</p>
                        </div>
                    </div>
                    <div className={'col-4 col-sm-6 col-md-6 flex p-3 end'}>
                        <div className={'searchDiv pt-2'}>
                            <FaSearch className={'search'}/>
                            <input type="text" placeholder={'Search anything...'}/>
                        </div>
                        <div className={'callDiv pt-2 position-relative'}>
                            <FaBell className={'bellIcon'}/>
                            <span
                                className="position-absolute top-0 start-150 translate-middle p-1 bg-secondary border border-light rounded-circle">
                            <span className="visually-hidden">New alerts</span>
                            </span>
                        </div>
                        <div className={'imgSection'}>
                            <img src="https://bazar-react.vercel.app/assets/images/avatars/001-man.svg" height={'36px'}
                                 alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>
}

export default Header