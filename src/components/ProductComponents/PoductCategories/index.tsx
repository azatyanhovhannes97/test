import React from "react";
import CategoryTable from "../../Tables/CategoryTable";
import GetCategory from "../../../hooks/GetCategory";

const ProductCategories: React.FC = () => {
     const { category,total } = GetCategory()
    return <>
        <CategoryTable thead={['id','name','createdAt','updatedAt','action']} product={category} />
    </>
}

export default ProductCategories