import * as Yup from 'yup';
export const categorySchema = Yup.object().shape({
    name: Yup.string()
        .min(2, "Too Short!")
        .max(30, "Too Long!")
        .required("Name is required"),

});