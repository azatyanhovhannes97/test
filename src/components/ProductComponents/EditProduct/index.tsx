import React, {useEffect} from "react";
import Input from "../../forms/Input";
import Select from "../../forms/Select";
import TextArea from "../../forms/TextArea";
import {toast, ToastContainer} from "react-toastify";
import {useParams} from "react-router-dom";
import {editItem} from "../../editItem";
import {useSelector} from "react-redux";
import {IState} from "../../../types/types";
import GetCategory from "../../../hooks/GetCategory";
import GetBrand from "../../../hooks/GetBrand";
import {FormikProps, useFormik} from "formik";
import {IFormik} from "../CreateProduct/types";
import {productSchema} from "../CreateProduct/productScema";

const EditProduct: React.FC = () => {


    const resolveAfter3Sec = new Promise((resolve, reject) => setTimeout(resolve,3000));
    const rejectAfter3Sec = new Promise((resolve, reject) => setTimeout(reject,3000))
    const success = () => toast.promise(
        resolveAfter3Sec,
        {
            pending: 'Promise is pending',
            success: `success add 👌`,
            error: `error'🤯'`
        }
    )
    const error = () => toast.promise(
        rejectAfter3Sec,
        {
            pending: 'Promise is pending',
            success: `success add 👌`,
            error: `error'🤯'`
        }
    )


    const params = useParams()
    editItem(params.id, "products")
    const {product} = useSelector((state: IState) => state.edit)
    const {category} = GetCategory()
    const {brand} = GetBrand()


   useEffect(() => {
       if (product[0]) {
           formik.setValues({
               name: product[0].name,
               description: product[0].description,
               image: product[0].image,
               price: product[0].price,
               stock: product[0].stock,
               category: product[0].categories[0].id,
               brand: product[0].brand.id
           })
       }
   }, [product])



    const getBase64 = async (event: React.ChangeEvent<any>) => {
        let file = await event.target.files[0];
        let reader = await new FileReader();
        await reader.readAsDataURL(file);
        console.log(URL.createObjectURL(event.target.files[0]))
        reader.onload = function () {
            formik.setValues({
                ...formik.values,
                image: String(reader.result)
            })
            return reader.result
        };

        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }


    const formik: FormikProps<IFormik>  = useFormik<IFormik>({
        initialValues: {
            name: '',
            description: "",
            image: "",
            price: 0,
            stock: 0,
            category:0,
            brand:0
        },
        validationSchema:productSchema,
        onSubmit: (values) =>{

            fetch(`${process.env.REACT_APP_BASE_URL}/products/${params.id}`,{
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({

                "name": values.name,
                "description": values.description,
                "price": values.price,
                "stock": values.stock,
                "image": values.image,
                "brandId": values.brand,
                "categoriesIds": [
                values.category
            ]


                })
            }).then(res => res.json())
                .then(res => {
                    success()
                }).catch(e => {
                    error()
            })

        }
    })



    return<>
            <form onSubmit={formik.handleSubmit}>
                <div className={'col-12 col-sm-12 col-md12 mt-3'}>
                    <div className={'container'}>
                        <div className="row g-3 mt-5">
                            <div className="col-sm-6">
                                <Input type={"text"} id={"name"} value={formik.values.name} className={`block w-full rounded border py-1 px-2 ${formik.touched.name && formik.errors.name ? 'is-invalid' : 'border-gray-300'}`}  placeholder={"Name"}
                                       onChange={formik.handleChange as () =>void} touched={formik.touched.name} errors={formik.errors.name} onBlur={formik.handleBlur as ()=> void}
                                />
                            </div>
                            <div className="col-sm-6">
                                <Select id={'category'} value={formik.values.category} onChange={(event) => {
                                    formik.setFieldValue('category', +(event?.currentTarget?.value || 0))
                                }
                                } options={category} placeholder={"Select Category"}/>
                            </div>
                        </div>
                        <div className="mb-3 mt-3 choose" onChange={getBase64}>
                            <input   type={"file"} id={"image"} onChange={formik.handleChange as () =>void} className={formik.touched.image && formik.errors.image ? 'is-invalid' : 'is-valid'}  accept=".jpeg,.png,.jpg"/>
                            <img src={formik.values.image}/>
                        </div>
                        <TextArea id={"description"} value={formik.values.description} className={`block w-full rounded border py-1 px-2 ${formik.touched.description && formik.errors.description ? 'is-invalid' : 'border-gray-300'}`}  placeholder={"Description"} required={"required"}
                                  onChange={formik.handleChange as () =>void} touched={formik.touched.description} errors={formik.errors.description} onBlur={formik.handleBlur as ()=> void}/>
                        <div className="row g-3 mt-3">
                            <div className="col-sm-6">
                                <Input type={"text"} id={"stock"} value={formik.values.stock} className={`block w-full rounded border py-1 px-2 ${formik.touched.stock && formik.errors.stock ? 'is-invalid' : 'border-gray-300'}`}  placeholder={"Stock"} required={"required"}
                                       onChange={formik.handleChange as () =>void} touched={formik.touched.stock} errors={formik.errors.stock} onBlur={formik.handleBlur as ()=> void}/>
                            </div>
                            <div className="col-sm-6">
                                <Input type={"number"} id={"price"} value={formik.values.price} className={`block w-full rounded border py-1 px-2 ${formik.touched.price && formik.errors.price ? 'is-invalid' : 'border-gray-300'}`}  placeholder={"Price"} required={"required"}
                                       onChange={formik.handleChange as () =>void} touched={formik.touched.price} errors={formik.errors.price} onBlur={formik.handleBlur as ()=> void}/>
                            </div>
                        </div>
                        <div className={'row mt-3'}>
                            <div className="col-sm-6">
                                <Select id={'brand'} value={formik.values.brand} onChange={(event) => {
                                    formik.setFieldValue('brand', +(event?.currentTarget?.value || 0))
                                }
                                } options={brand} placeholder={"Select Brand"}/>
                            </div>
                        </div>

                        <div>
                            <button type="submit" className="btn btn-primary mt-5" >Save Product</button>
                            <ToastContainer
                                position="top-right"
                                autoClose={5000}
                                hideProgressBar={false}
                                newestOnTop={false}
                                closeOnClick
                                rtl={false}
                                pauseOnFocusLoss
                                draggable
                                pauseOnHover
                                theme="light"
                            />
                        </div>
                    </div>
                </div>
            </form>

    </>
}

export default EditProduct