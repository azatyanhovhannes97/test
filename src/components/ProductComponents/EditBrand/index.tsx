import React, {useEffect} from "react";
import {FormikProps, useFormik} from "formik";
import {IFormik} from "../CreateProduct/types";
import {BrandSchema} from "../CreateBrand/BrandSchema";
import Input from "../../forms/Input";
import {useSelector} from "react-redux";
import {IState} from "../../../types/types";
import {editItem} from "../../editItem";
import {useParams} from "react-router-dom";
import {toast, ToastContainer} from "react-toastify";

const EditBrand: React.FC = () => {

    const resolveAfter3Sec = new Promise((resolve, reject) => setTimeout(resolve,3000));
    const rejectAfter3Sec = new Promise((resolve, reject) => setTimeout(reject,3000))
    const success = () => toast.promise(
        resolveAfter3Sec,
        {
            pending: 'Promise is pending',
            success: `success add 👌`,
            error: `error'🤯'`
        }
    )
    const error = () => toast.promise(
        rejectAfter3Sec,
        {
            pending: 'Promise is pending',
            success: `success add 👌`,
            error: `error'🤯'`
        }
    )

    const params = useParams()
    editItem(params.id,'brands')
    const { brand } = useSelector((state: IState) => state.edit)
    console.log(brand)

    useEffect(() => {
        if (brand[0]) {
            formik.setValues({
                ...formik.values,
                name: brand[0].name,
                image: brand[0].image
            })
        }
    },[brand])

    const getBase64 = async (event: React.ChangeEvent<any>) => {
        let file = await event.target.files[0];
        let reader = await new FileReader();
        await reader.readAsDataURL(file);
        console.log(URL.createObjectURL(event.target.files[0]))
        reader.onload = function () {
            formik.setValues({
                ...formik.values,
                image: String(reader.result)
            })
            return reader.result
        };

        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }

    const formik: FormikProps<IFormik>  = useFormik<IFormik>({
        initialValues: {
            name: "",
            description: "",
            image: "",
            price: 0,
            stock: 0,
            category:0,
            brand:0

        },
        validationSchema:BrandSchema,
        onSubmit: (values) => {
            fetch(`${process.env.REACT_APP_BASE_URL}/brands/${params.id}`,{
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "name": values.name,
                    "image": values.image


                })
            }).then(res => res.json())
                .then(res => {
                    success()
                }).catch(e => {
                    error()
            })
        }
    })
    return<>
        <form action="" onSubmit={formik.handleSubmit}>
            <div className={"container"}>
                <div className={'row'}>
                    <div className={'col-md-12 mt-5'}>
                        <Input type={"text"} id={"name"} value={formik.values.name} className={`block w-full rounded border py-1 px-2 ${formik.touched.name && formik.errors.name ? 'is-invalid' : 'border-gray-300'}`}  placeholder={"Name"}
                               onChange={formik.handleChange as () =>void} touched={formik.touched.name} errors={formik.errors.name} onBlur={formik.handleBlur as ()=> void}
                        />
                    </div>
                    <div className={'col-md-12 mt-5'} onChange={getBase64}>
                        <input   type={"file"} id={"image"} onChange={formik.handleChange as () =>void} className={formik.touched.image && formik.errors.image ? 'is-invalid' : 'is-valid'}  accept=".jpeg,.png,.jpg"/>
                        <img src={formik.values.image} width={'30px'} height={'30px'}/>
                    </div>
                    <button type="submit" className="btn btn-primary mt-5" >Save Product</button>
                    <ToastContainer
                        position="top-right"
                        autoClose={5000}
                        hideProgressBar={false}
                        newestOnTop={false}
                        closeOnClick
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover
                        theme="light"
                    />
                </div>
            </div>
        </form>
    </>

}

export default EditBrand