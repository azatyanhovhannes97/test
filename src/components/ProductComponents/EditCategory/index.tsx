import React, {useEffect} from "react";
import {FormikProps, useFormik} from "formik";
import {IFormik} from "../CreateProduct/types";
import {categorySchema} from "../CreateCategory/categorySchema";
import Input from "../../forms/Input";
import {useParams} from "react-router-dom";
import {useSelector} from "react-redux";
import {IState} from "../../../types/types";
import {editItem} from "../../editItem";
import {toast, ToastContainer} from "react-toastify";

const EditCategory: React.FC = () => {

    const resolveAfter3Sec = new Promise((resolve, reject) => setTimeout(resolve,3000));
    const rejectAfter3Sec = new Promise((resolve, reject) => setTimeout(reject,3000))
    const success = () => toast.promise(
        resolveAfter3Sec,
        {
            pending: 'Promise is pending',
            success: `success add 👌`,
            error: `error'🤯'`
        }
    )
    const error = () => toast.promise(
        rejectAfter3Sec,
        {
            pending: 'Promise is pending',
            success: `success add 👌`,
            error: `error'🤯'`
        }
    )

    const params = useParams()
    editItem(params.id,'categories')
    const {category} = useSelector((state: IState) => state.edit)


    useEffect(() => {
        if (category[0]){
            formik.setValues({
                ...formik.values,
                name: category[0].name
            })
        }
    },[category])

    const formik: FormikProps<IFormik>  = useFormik<IFormik>({
        initialValues: {
            name: "",
            description: "",
            image: "",
            price: 0,
            stock: 0,
            category:0,
            brand:0

        },
        validationSchema:categorySchema,
        onSubmit: (values) => {
            fetch(`${process.env.REACT_APP_BASE_URL}/categories/${params.id}`,{
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "name": values.name,


                })
            }).then(res => res.json())
                .then(res => {
                    success()
                }).catch(e => {
                    error()
            })
        }
    })

    return<>
        <form action="" onSubmit={formik.handleSubmit}>
            <div className={"container"}>
                <div className={'row'}>
                    <div className={'col-md-12 mt-5'}>
                        <Input type={"text"} id={"name"} value={formik.values.name} className={`block w-full rounded border py-1 px-2 ${formik.touched.name && formik.errors.name ? 'is-invalid' : 'border-gray-300'}`}  placeholder={"Name"}
                               onChange={formik.handleChange as () =>void} touched={formik.touched.name} errors={formik.errors.name} onBlur={formik.handleBlur as ()=> void}
                        />
                    </div>
                    <div className={'col-md-12'}>
                        <button type="submit" className="btn btn-primary mt-5" >Save Category</button>
                        <ToastContainer
                            position="top-right"
                            autoClose={5000}
                            hideProgressBar={false}
                            newestOnTop={false}
                            closeOnClick
                            rtl={false}
                            pauseOnFocusLoss
                            draggable
                            pauseOnHover
                            theme="light"
                        />
                    </div>
                </div>
            </div>
        </form>
    </>
}

export default EditCategory