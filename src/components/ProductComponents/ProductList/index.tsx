import React from "react";
import GetData from "../../../hooks/getData";
import Table from "../../Tables/Table";


const ProductList: React.FC = () => {
    const {product, total} = GetData()


    return<>
    <Table thead={['Name','Category.Name','Brand.Image','Price','Action']} product={product} symbols={['','','','$','']} />
    </>
}

export default ProductList