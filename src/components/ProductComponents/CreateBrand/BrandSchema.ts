import * as Yup from 'yup';
export const BrandSchema = Yup.object().shape({
    name: Yup.string()
        .min(2, "Too Short!")
        .max(30, "Too Long!")
        .required("Name is required"),
    image: Yup.string()
        .min(1, "Too Short!")
        .max(2000000, "Too Long!")
        .required("Image is required"),

});