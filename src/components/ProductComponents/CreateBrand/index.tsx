import React from "react";
import {FormikProps, useFormik} from "formik";
import {IFormik} from "../CreateProduct/types";
import {BrandSchema} from "./BrandSchema";
import Input from "../../forms/Input";
import {toast, ToastContainer} from "react-toastify";

const CreateBrand: React.FC = () => {

    const resolveAfter3Sec = new Promise((resolve, reject) => setTimeout(resolve,3000));
    const rejectAfter3Sec = new Promise((resolve, reject) => setTimeout(reject,3000))
    const success = () => toast.promise(
        resolveAfter3Sec,
        {
            pending: 'Promise is pending',
            success: `success add 👌`,
            error: `error'🤯'`
        }
    )
    const error = () => toast.promise(
        rejectAfter3Sec,
        {
            pending: 'Promise is pending',
            success: `success add 👌`,
            error: `error'🤯'`
        }
    )


    const getBase64 = async (event: React.ChangeEvent<any>) => {
        let file = await event.target.files[0];
        let reader = await new FileReader();
        await reader.readAsDataURL(file);
        console.log(URL.createObjectURL(event.target.files[0]))
        reader.onload = function () {
            formik.setValues({
                ...formik.values,
                image: String(reader.result)
            })
            return reader.result
        };

        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }

    const formik: FormikProps<IFormik>  = useFormik<IFormik>({
        initialValues: {
            name: "",
            description: "",
            image: "",
            price: 0,
            stock: 0,
            category:0,
            brand:0

        },
        validationSchema:BrandSchema,
        onSubmit: (values) => {
            fetch(`${process.env.REACT_APP_BASE_URL}/brands`,{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "name": values.name,
                    "image": values.image


                })
            }).then(res => res.json())
                .then(res => {
                    success()
                }).catch(e => {
                    error()
            })
        }
    })
    return<>
        <form action="" onSubmit={formik.handleSubmit}>
            <div className={"container"}>
                <div className={'row'}>
                    <div className={'col-md-12 mt-5'}>
                        <Input type={"text"} id={"name"} value={formik.values.name} className={`block w-full rounded border py-1 px-2 ${formik.touched.name && formik.errors.name ? 'is-invalid' : 'border-gray-300'}`}  placeholder={"Name"}
                               onChange={formik.handleChange as () =>void} touched={formik.touched.name} errors={formik.errors.name} onBlur={formik.handleBlur as ()=> void}
                        />
                    </div>
                    <div className={'col-md-12 mt-5'} onChange={getBase64}>
                        <input   type={"file"} id={"image"} onChange={formik.handleChange as () =>void} className={formik.touched.image && formik.errors.image ? 'is-invalid' : 'is-valid'}  accept=".jpeg,.png,.jpg"/>
                        <img src={formik.values.image} height={'30px'} width={'30px'}/>
                    </div>
                    <button type="submit" className="btn btn-primary mt-5" >Save Product</button>
                    <ToastContainer
                        position="top-right"
                        autoClose={5000}
                        hideProgressBar={false}
                        newestOnTop={false}
                        closeOnClick
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover
                        theme="light"
                    />
                </div>
            </div>
        </form>
    </>
}

export default CreateBrand