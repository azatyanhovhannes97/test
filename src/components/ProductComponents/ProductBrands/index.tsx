import React from "react";
import GetBrand from "../../../hooks/GetBrand";
import BrandTable from "../../Tables/BrandTable";

const ProductBrands: React.FC = () => {
    const { brand,total } = GetBrand()

    return<>
    <BrandTable thead={['id', 'name', 'image', 'createdAt', 'updatedAt','action']} product={brand} />
    </>
}

export default ProductBrands