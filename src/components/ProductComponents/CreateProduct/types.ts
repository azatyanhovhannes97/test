export interface IFormik {
    name?:string,
    description?:string,
    price?:number,
    stock?:number,
    image?: string,
    category:number,
    brand:number
}