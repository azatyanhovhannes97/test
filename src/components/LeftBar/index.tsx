import "./style.css"
import React, {useMemo} from "react";
import { Link } from "react-router-dom"
import Dash from "../svg/Dash";
import Products from "../svg/Products";
import Orders from "../svg/orders";
import {useLocation} from "react-router-dom";
import Header from "../Header";
import { useSelector, useDispatch } from "react-redux";
import {IState} from "../../types/types";
import { changePar } from "../../app/feaures/clickState/clickState";


const LeftBar: React.FC = () => {
    let path = useLocation().pathname
    const mobileClassHook = useSelector((state: IState) => state.click.click)
    const dispatch = useDispatch()



    return <>
        <div className={`left_bar ${mobileClassHook === false ? 'left_bar1' : 'left_bar2'}`}>
        <div className={'container'}>
            <div className={'row'}>
                <div className={'col-md-12'}>
                    <div className={'store'}>
                        <img src="https://bazar-react.vercel.app/assets/images/logo.svg" alt=""/>

                    </div>

                </div>
                <div className={'col-md-12'}>
                    <div>
                        <p className={'admin'}>ADMIN</p>
                    </div>
                    <div>

                        <Link onClick={() => { dispatch(changePar(!mobileClassHook)) }} to={'/'} className={`flex LeftLink ${path === '/' ? 'thisLocation' : ''}`}><Dash className={'leftBarIcon'}/> <span>Dashboaed</span></Link>
                        <Link onClick={() => { dispatch(changePar(!mobileClassHook)) }} to={'/products'} className={`flex LeftLink ${path === '/products' ? 'thisLocation' : ''}`}><Products className={'leftBarIcon'}/> <span>Products</span></Link>
                        <Link onClick={() => { dispatch(changePar(!mobileClassHook)) }} to={'/products/categories'} className={`flex LeftLink ${path === '/products/categories' ? 'thisLocation' : ''}`}><Products className={'leftBarIcon'}/> <span>Category</span></Link>
                        <Link onClick={() => { dispatch(changePar(!mobileClassHook)) }} to={'/products/brands'} className={`flex LeftLink ${path === '/products/brands' ? 'thisLocation' : ''}`}><Products className={'leftBarIcon'}/> <span>Brand</span></Link>
                        <Link onClick={() => { dispatch(changePar(!mobileClassHook)) }} to={'/orders'} className={`flex LeftLink ${path === '/orders' ? 'thisLocation' : ''}`}><Orders className={'leftBarIcon'}/> <span>Orders</span></Link>

                    </div>
                </div>
            </div>
        </div>
        </div>
    </>
}

export default LeftBar