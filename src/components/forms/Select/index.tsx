import React from "react";
import {IProps1} from "../types/types";

const Select: React.FC<IProps1> = (props:IProps1) => {

    return <>
        <select value={props.value} className="form-select" id={props.id} onChange={props.onChange}>
            <option>{props.placeholder}</option>
            {props.options?.map((item) => {

                return <React.Fragment key={item.id}><option value={item.id}>{item.name}</option></React.Fragment>
            })}

        </select>
    </>
}

export default Select