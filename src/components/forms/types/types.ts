export interface IProps {
    type?: string,
    id?: string,
    placeholder?: string,
    required?: string,
    options?:IOption[],
    onChange?: () => void,
    onBlur?:()=>void,
    value?:string | number,
    className?:string,
    touched?:boolean,
    errors?:string
}
interface IOption {
    "id"?: number,
    "name"?: string,
    "createdAt"?: string,
    "updatedAt"?: string
}
export interface IProps1 {
    type?: string,
    id?: string,
    placeholder?: string,
    required?: string,
    options?:IOption[],
    onChange?: (event?: React.FormEvent<HTMLSelectElement>) => void,
    onBlur?:()=>void,
    value?:string | number,
    className?:string,
    touched?:boolean,
    errors?:string
}