import "./style.css"
import React from "react";
import {IProps} from "../types/types";

const TextArea: React.FC<IProps> = (props: IProps) => {
    return<>
        <div className="mb-3 mt-3">
            <textarea  id={props.id} className={`form-control ${props.className}`} name={props.id} placeholder={props.placeholder}
                      onChange={props.onChange} onBlur={props.onBlur} value={props.value} />
            {props.touched && props.errors && (
                <span className='text-red-400'>{props.errors}</span>
            )}
        </div>
    </>
}

export default TextArea