import React from "react";
import {IProps} from "../types/types";

const Input: React.FC<IProps> = (props:IProps) => {
    return <>
        <input type={props.type} className={`form-control ${props.className}`} name={props.id} id={props.id} placeholder={props.placeholder}
               onChange={props.onChange} onBlur={props.onBlur} value={props.value}
        />
        {props.touched && props.errors && (
            <span className='text-red-400'>{props.errors}</span>
        )}
    </>
}

export default Input