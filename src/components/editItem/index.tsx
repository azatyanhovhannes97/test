import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {IState} from "../../types/types";
import {ChangeBrand, ChangeCategory, ChangeProduct, setLoading} from "../../app/feaures/editData/edit";
import {IEditBrand, IEditCategory, IEditProduct} from "../../app/feaures/types/types";

export const editItem = (id:string | undefined, url: string) => {
    const data: any = []

    const dispatch = useDispatch()
    useEffect(() => {
        if(url === 'brands'){
            fetch(`${process.env.REACT_APP_BASE_URL}/${url}/${id}`)
                .then(res => res.json())
                .then((res: IEditBrand) => {
                    data.push(res)
                    dispatch(ChangeBrand([...data]))
                })
        }else if(url === 'products'){
            fetch(`${process.env.REACT_APP_BASE_URL}/${url}/${id}`)
                .then(res => res.json())
                .then((res : IEditProduct) => {
                    data.push(res)
                    dispatch(ChangeProduct([...data]))
                })
        }else if(url === 'categories'){
            fetch(`${process.env.REACT_APP_BASE_URL}/${url}/${id}`)
                .then(res => res.json())
                .then((res: IEditCategory) => {
                    data.push(res)
                    dispatch(ChangeCategory([...data]))
                })
        }
    },[])



}

