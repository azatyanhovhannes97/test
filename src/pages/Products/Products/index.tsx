import React from "react";
import PageHead from "../../../components/PageHead";
import ProductList from "../../../components/ProductComponents/ProductList";

const Products: React.FC = () => {
    return<>
        <div className={"container"}>
            <div className={'row orders_parent'}>
                <PageHead head={"Product List"} input={'Product'} button={'Add'} to={'/addProduct'}/>
                <div className={'col-12 col-sm-12 col-md12'}>
                    <ProductList/>
                </div>
            </div>
        </div>
    </>
}

export default Products