import React from "react";
import PageHead from "../../../components/PageHead";
import ProductBrands from "../../../components/ProductComponents/ProductBrands";

const Brands: React.FC = () => {

    return<>
        <div className={'container'}>
            <div className={'row mt-5'}>
        <PageHead head={'Product Brands'} input={'Brand'} button={'Add'} to={'/addBrand'}/>
        <div className={'col-12 col-sm-12 col-md12 mt-3'}>
            <ProductBrands />
        </div>
            </div>
        </div>

    </>
}

export default Brands