import React from "react";
import PageHead from "../../../components/PageHead";
import ProductCategories from "../../../components/ProductComponents/PoductCategories";

const Category: React.FC = () => {
    return<>
        <div className={'container'}>
            <div className={'row mt-5'}>
                <PageHead head={"Product Categories"} input={'Category'} button={'Add'} to={'/addCategory'}/>
                <div className={'col-12 col-sm-12 col-md12 mt-3'}>
                    <ProductCategories />
                </div>
            </div>
        </div>

    </>
}

export default Category