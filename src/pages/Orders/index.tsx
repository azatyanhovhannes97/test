import "./style.css"
import React from "react";
import PageHead from "../../components/PageHead";

const Orders: React.FC = () => {
    return <>
        <div className={"container"}>
            <div className={'row orders_parent'}>
               <PageHead head={"Orders"} input={'Order'} button={'Create'}/>
                <div className={'col-12 col-sm-12 col-md12'}></div>
                <PageHead head={'Order Details'}/>
            </div>
        </div>
    </>
}

export default Orders