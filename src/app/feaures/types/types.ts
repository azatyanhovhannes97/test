export interface IClick {
    click: boolean
}
export interface IICategories{
    product: ICategories[]
    total: number
}
export interface IIBrands {
    product: IBrand[]
    total: number
}

export interface IEdit{
    product: IEditProduct[],
    brand:IEditBrand[],
    category: IEditCategory[],
    loading:boolean
}

export interface IEditProduct{
    brand: {id: number, name: string, image: string, createdAt: string, updatedAt: string}
    categories: ICategories[]
    createdAt: string
    description: string
    id: number
    image: string
    name: string
    price: number
    stock: number
    updatedAt: string
}

export interface IEditCategory {
    "id": number,
    "name": string,
    "products":IProduct[],
    "createdAt": string,
    "updatedAt": string
}

export interface IEditBrand{
    createdAt: string
    id: number
    image: string
    name: string
    products: IBrandProduct[]
    updatedAt: string
}

export interface IBrandProduct {
    createdAt: string
    description: string
    id: number
    image: string
    name: string
    price: number
    stock: number
    updatedAt: string
}

export interface ICategories {
    "id": number,
    "name": string,
    "createdAt": string,
    "updatedAt": string
}

export interface IData {
    product:IProduct[],
    total: number

}

export interface IBrand {
    "id"?: number,
    "name"?: string,
    "image"?: string,
    "createdAt"?: string,
    "updatedAt"?: string
}
export interface IProduct {

            "id"?: number,
            "name"?: string,
            "description"?: string,
            "price"?: number,
            "stock"?: number,
            "image"?: string,
            "createdAt"?: string,
            "updatedAt"?: string,
            "brand"?: IBrand,
            "categories"?: ICategories[],

}