import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import {IEdit, IEditBrand, IEditCategory, IEditProduct} from "../types/types";

const initialState: IEdit = {
    product: [],
    category: [],
    brand: [],
    loading:true
}

export const ChangeEdit = createSlice({
    name: 'edit',
    // `createSlice` will infer the state type from the `initialState` argument
    initialState,
    reducers: {
        ChangeProduct: (state:IEdit, action:PayloadAction<IEditProduct[]>) => {
            state.product = [...action.payload]
        },
        ChangeCategory: (state: IEdit, action: PayloadAction<IEditCategory[]>) => {
            state.category = [...action.payload]
        },
        ChangeBrand: (state:IEdit, action:PayloadAction<IEditBrand[]>) => {
            state.brand = [...action.payload]
        },
        setLoading: (state:IEdit, action:PayloadAction<boolean>) => {
            state.loading = action.payload
        }


    },
})

export const { ChangeProduct, ChangeCategory, ChangeBrand,setLoading } = ChangeEdit.actions

export default ChangeEdit.reducer