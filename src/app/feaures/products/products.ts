import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import {IData, IProduct} from "../types/types";

const initialState: IData = {
    product:[],
    total: 10
}


export const GetProducts = createSlice({
    name: 'products',

    initialState,
    reducers: {
        changeProduct: (state:IData,action: PayloadAction<IProduct[]>) => {
            state.product = [...action.payload]
        },
        changeTotal: (state:IData,action:PayloadAction<number>) => {
            state.total = action.payload
        }

    },
})


export const  { changeProduct, changeTotal } = GetProducts.actions

export default GetProducts.reducer