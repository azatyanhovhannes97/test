import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import {IClick} from "../types/types";


const initialState: IClick = {
    click: false,
}

export const clickSlise = createSlice({
    name: 'click',
    // `createSlice` will infer the state type from the `initialState` argument
    initialState,
    reducers: {
        changePar: (state,action: PayloadAction<boolean>) => {
            state.click = action.payload
        },

    },
})

export const { changePar } = clickSlise.actions


export default clickSlise.reducer