import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IBrand, IIBrands, } from "../types/types";
import moment from "moment";

const initialState: IIBrands = {
    product: [],
    total: 5
}

export const ChangeBrands = createSlice({
    name: 'brands',
    // `createSlice` will infer the state type from the `initialState` argument
    initialState,
    reducers: {
        changeProduct: (state:IIBrands,action: PayloadAction<IBrand[]>) => {
            state.product = [...action.payload]
            state.product.map(item => {
               item.createdAt = moment(item.createdAt).format('ll')
                item.updatedAt = moment(item.updatedAt).format('ll')
            })
        },
        changeTotal: (state: IIBrands,action:PayloadAction<number>) => {
            state.total = action.payload
        }


    },
})


export const { changeProduct, changeTotal } = ChangeBrands.actions

export default ChangeBrands.reducer