import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import {IICategories, ICategories} from "../types/types";
import {clickSlise} from "../clickState/clickState";
import moment from "moment";

const initialState: IICategories = {
    product: [],
    total: 5
}

export const ChangeCategories = createSlice({
    name: 'categories',
    // `createSlice` will infer the state type from the `initialState` argument
    initialState,
    reducers: {
        changeProduct: (state:IICategories,action: PayloadAction<ICategories[]>) => {
            state.product = [...action.payload]
            state.product.map(item => {
                item.createdAt = moment(item.createdAt).format('ll')
                item.updatedAt = moment(item.updatedAt).format('ll')
            })
        },
        changeTotal: (state: IICategories,action:PayloadAction<number>) => {
            state.total = action.payload
        }


    },
})

export const { changeProduct, changeTotal } = ChangeCategories.actions


export default ChangeCategories.reducer