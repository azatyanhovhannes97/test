import { configureStore } from '@reduxjs/toolkit'
import clickReduser from "./feaures/clickState/clickState"
import categoryReduser from "./feaures/categories/categories"
import productReduser from "./feaures/products/products"
import brandReduser from "./feaures/brands/brands"
import editReduser from "./feaures/editData/edit"

export default configureStore({
    reducer: {
        click: clickReduser,
        category: categoryReduser,
        product: productReduser,
        brand: brandReduser,
        edit: editReduser
    },
})


