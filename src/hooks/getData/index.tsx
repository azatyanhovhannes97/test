import React, {useCallback, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {IState} from "../../types/types";
import {IData} from "../../app/feaures/types/types";
import {changeTotal,changeProduct} from "../../app/feaures/products/products";

const GetData = () => {
    const {product, total } = useSelector((state:IState) => state.product)
    const dispatch = useDispatch()

    const sendRequest = useCallback(() =>{
        try {
            fetch(`${process.env.REACT_APP_BASE_URL}/products`)
                .then(res => res.json())
                .then((res: IData) => {
                    dispatch(changeProduct([...res.product]))
                    dispatch(changeTotal(res.total))
                })
        }catch (e) {

        }
    },[])

    useEffect(() => {

            sendRequest()

    },[])

    return{
        product,
        total,
        sendRequest
    }
}

export default GetData