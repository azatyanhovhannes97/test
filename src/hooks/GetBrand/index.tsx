import React, {useCallback, useEffect} from "react";
import { useSelector,useDispatch } from "react-redux";
import {changeProduct, changeTotal} from "../../app/feaures/brands/brands";
import {IState} from "../../types/types";
import {IIBrands} from "../../app/feaures/types/types";

const GetBrand = () => {
    const { product, total } = useSelector((state: IState) => state.brand)
    const dispatch = useDispatch()

    const sendRequest = useCallback(() => {
        try {
            fetch(`${process.env.REACT_APP_BASE_URL}/brands`)
                .then(res => res.json())
                .then((res: IIBrands) => {
                    dispatch(changeProduct([...res.product]))
                    dispatch(changeTotal(res.total))
                })
        }catch (e) {

        }
    }, [])

    useEffect(() => {
        sendRequest()
    },[])

    return {
        brand:product,
        total,
        sendRequest
    }

}

export default GetBrand