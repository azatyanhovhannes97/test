import React, {useCallback, useEffect} from "react";
import { changeProduct, changeTotal } from "../../app/feaures/categories/categories";
import { useSelector,useDispatch } from "react-redux";
import {IState} from "../../types/types";
import {IICategories,ICategories} from "../../app/feaures/types/types";
import * as Process from "process";

const GetCategory = () => {
    const dispatch = useDispatch()
    const {product,total} = useSelector((state: IState) => state.category)

    const sendRequest = useCallback(() => {
        try {
            fetch(`${process.env.REACT_APP_BASE_URL}/categories`)
                .then(res => res.json())
                .then((res: IICategories) => {
                    dispatch(changeProduct([...res.product]))
                    dispatch(changeTotal(res.total))
                })

        }catch (e){

        }
    }, [])

    useEffect(() => {
        sendRequest()
    },[])

    return {
        category: product,
        total,
        sendRequest
    }
}

export default GetCategory