import "./assets/css/style.css"
import "bootstrap/dist/css/bootstrap.css"
import React from 'react';
import LeftBar from "./components/LeftBar"
import {Route, Routes} from "react-router-dom";
import Home from "./pages/Home";
import Orders from "./pages/Orders";
import Products from "./pages/Products/Products";
import Header from "./components/Header";
import Brands from "./pages/Products/Brands";
import Category from "./pages/Products/Category";
import CreateProduct from "./components/ProductComponents/CreateProduct";
import CreateCategory from "./components/ProductComponents/CreateCategory";
import CreateBrand from "./components/ProductComponents/CreateBrand";
import EditProduct from "./components/ProductComponents/EditProduct";
import EditBrand from "./components/ProductComponents/EditBrand";
import EditCategory from "./components/ProductComponents/EditCategory";

function App() {
  return <>

    <div className={'flex parent'}>
    <LeftBar/>
      <div className={'right_window'}>
    <Header/>
    <Routes>
      <Route path={'/'} element={<Home/>} />
      <Route path={'/orders'} element={<Orders/>} />
      <Route path={'/products'} element={<Products/>} />
      <Route path={'/products/brands'} element={<Brands/>} />
      <Route path={'/products/categories'} element={ <Category/> } />
      <Route path={'/addProduct'} element={ <CreateProduct /> } />
      <Route path={'/addCategory'} element={<CreateCategory />} />
      <Route path={'/addBrand'} element={<CreateBrand />} />
      <Route path={'/editProduct/:id'} element={ <EditProduct /> } />
      <Route path={'/editBrand/:id'} element={<EditBrand />} />
      <Route path={'editCategory/:id'} element={<EditCategory />} />
    </Routes>
      </div>
    </div>

  </>

}

export default App;
